import React, { useState } from "react";
import { Container, Row, Col } from "react-grid-system";
import {
    InputGroup,
    InputGroupButtonDropdown,
    DropdownItem,
    FormGroup,
    Label,
    Input,
    DropdownToggle,
    DropdownMenu,
    ButtonToggle,
} from "reactstrap";
//import ReactDatetime from "react-datetime";
import * as utils from "../services/axios-functions";
import "moment/locale/es";
import imagePersona2 from "../assets/images/persona2.png";
import Header from "../pages/Header";
import { Redirect, useHistory } from "react-router-dom";

function HomePage(props) {

    const [email, setEmail] = useState("");
    const history = useHistory();

    React.useEffect(() => {
        const functions_first = async () => {
            if (!localStorage.getItem("response")) {
                return <Redirect to={"/home"} />;
            }
            let person = JSON.parse(localStorage.getItem("response"))
            setEmail(person.email);
        };
        functions_first();
    }, []);

    if (!localStorage.getItem("response")) {
        return <Redirect to={"/home"} />;
    }

    return (
        <>
            <Header />
            <Container fluid>
                <Row>
                    <Col md={5} sm={12} lg={5}>
                        <img src={imagePersona2} style={{ height: "80vh", display: "flex", justifyContent: "center", alignItems: "center" }} alt="Imagen Home" class="img-fluid" />
                    </Col>
                    <Col md={7} sm={12} lg={7}>
                        <br />
                        <br />
                        <br />
                        <div style={{ margin: "0 8rem" }}>
                            <span style={{ color: "#ef3340", fontSize: "36px" }}>¡Te damos la bienvenida!</span>
                            <h3 style={{ fontWeight: "400" }}>
                                Cuanta con nosotros para proteger tu vehículo
                            </h3>
                        </div>
                        <div style={{ margin: "0 8rem", color: "#676F8F" }}>
                            <p style={{ fontSize: "16px" }}>Enviaremos la confirmación de compra de tu Plan Vehicular Tracking a tu correo:</p>
                            <p>{email}</p>
                        </div>
                        <div style={{ margin: "0 8rem" }}>
                            <Input onClick={() => {
                                localStorage.removeItem("person");
                                localStorage.removeItem("response");
                                history.push("home");
                            }} type="button" value="CÓMO USAR MI SEGURO" style={{ width: "15rem", height: "4rem", backgroundColor: "#EF3340", color: "#FFF", borderRadius: "8px" }} />
                        </div>
                    </Col>
                </Row>
            </Container>
        </>
    );
}

export default HomePage;

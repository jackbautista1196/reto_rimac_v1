import React, { useState } from "react";
import { Container, Row, Col } from "react-grid-system";
import {
  InputGroup,
  Input,
  Card,
  CardBody
} from "reactstrap";
import "moment/locale/es";
import Header from "../pages/Header";
import { Redirect, useHistory } from "react-router-dom";
import imagePersona from "../assets/images/persona.png";

function Step1(props) {

  const [person, setPerson] = useState({});
  const [dataApi, setDataApi] = useState({});
  const [sumaTotal, setSumaTotal] = useState(0);
  const history = useHistory();
  React.useEffect(() => {
    const functions_first = async () => {
      if (!localStorage.getItem("response")) {
        return <Redirect to={"/home"} />;
      }
      setDataApi(JSON.parse(localStorage.getItem("response")));
      setPerson(JSON.parse(localStorage.getItem("person")));
    };
    functions_first();
  }, []);

  const increases = () => {
    let s = sumaTotal;
    s = s + 100;
    setSumaTotal(s);
  }

  const diminish = () => {
    let s = sumaTotal;
    s = s - 100;
    if (s < 0) {
      setSumaTotal(0);
    } else {
      setSumaTotal(s);
    }
  }

  if (!localStorage.getItem("response")) {
    return <Redirect to={"/home"} />;
  }

  return (
    <>
      <Header />
      <Container fluid>
        <Row style={{
          height: "88vh"
        }}>
          <Col md={4} sm={12} lg={4} style={{ background: "#F7F8FC" }}>
            <br />
            <br />
            <div style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
            }}>
              <div style={{
                display: "flex",
                justifyContent: "flex-start",
                alignItems: "center",
                width: "15rem"
              }}>
                <div style={{
                  width: "2rem",
                  height: "2rem",
                  borderRadius: "50%",
                  border: "1px solid #A3ABCC",
                  background: "white",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  textAlign: "center",
                  padding: "3%",
                  color: "#A3ABCC"
                }}><span>1</span>
                </div>
                <div style={{ margin: "0 1rem" }}>
                  <span style={{ color: "#A3ABCC" }}>Datos</span>
                </div>
              </div>
            </div>
            <br />
            <br />
            <div style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
            }}>
              <div style={{
                display: "flex",
                justifyContent: "flex-start",
                alignItems: "center",
                width: "15rem"
              }}>
                <div style={{
                  width: "2rem",
                  height: "2rem",
                  borderRadius: "50%",
                  border: "1px solid #6F7DFF",
                  background: "#6F7DFF",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  textAlign: "center",
                  padding: "3%",
                  color: "#FFF"
                }}><span>2</span>
                </div>
                <div style={{ margin: "0 1rem" }}>
                  <span style={{ color: "#A3ABCC" }}>Arma tu plan</span>
                </div>
              </div>
            </div>
          </Col>
          <Col md={4} sm={12} lg={4}>
            <br />
            <br />
            <div style={{
              display: "flex",
              justifyContent: "flex-start",
              alignItems: "center",
              width: "15rem",
              cursor: "pointer"
            }} onClick={() => {
              localStorage.removeItem("response"); localStorage.removeItem("person");
              return <Redirect to={"/home"} />;
            }}>
              <div style={{
                width: "2rem",
                height: "2rem",
                borderRadius: "50%",
                border: "1px solid #FC606B",
                background: "white",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                textAlign: "center",
                padding: "3%",
                color: "#FC606B"
              }}><span>{"<"}</span>
              </div>
              <div style={{ margin: "0 1rem" }}>
                <span style={{ color: "#A3ABCC" }}>VOLVER</span>
              </div>
            </div>
            <br />
            <h2>
              ¡Hola
              <span style={{ color: "red" }}>
                {" "}

              </span>
              , {dataApi.name} !
            </h2>
            <p style={{ color: "#676F8F" }}>Conoce las coberturas para tu plan</p>
            <br />
            <Col sm={12} lg={12} md={12} style={{ padding: 0 }}>
              <Card body style={{ border: "1px solid #C5CBE0", borderRadius: "8px" }}>
                <CardBody style={{ padding: "0rem 1.25rem" }}>
                  <Row>
                    <Col sm={9} lg={9} md={9} style={{ margin: "2rem 0" }}>
                      <p style={{ fontSize: "12px" }}>Placa: {person.placa}</p>
                      <h4>Wolkswagen 2020</h4>
                      <h4>Golf</h4>
                    </Col>
                    <Col sm={3} lg={3} md={3} style={{ padding: 0 }}>
                      <div>
                        <img style={{
                          objectFit: "cover",
                          width: "17vw", height: "11vw", padding: "0vw"
                        }} src={imagePersona} alt="Imagen Home" class="img-fluid" />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
            <br />
            <Row>
              <Col sm={6} md={6} lg={6}>
                <p>Indica la suma asegurada</p>
                <p>
                  <Row>
                    <Col><p style={{ fontSize: "12px" }}>MIN $12,500</p></Col>
                    <Col><p style={{ fontSize: "12px" }}>MAX $16,500</p></Col>
                  </Row>
                </p>
              </Col>
              <Col sm={6} md={6} lg={6}>
                <InputGroup style={{}}>
                  <Input type="button" onClick={() => diminish()} value="-" style={{ borderRadius: "8px 0px 0px 8px", borderRight: 0, fontSize: "20px", height: "3rem" }} />
                  <Input type="text" value={`${(sumaTotal).toLocaleString('en-US', { style: 'currency', currency: 'USD' })}`} style={{ textAlign: "center", fontSize: "12px", height: "3rem", borderRight: 0, borderLeft: 0 }} />
                  <Input type="button" onClick={() => increases()} value="+" style={{ borderRadius: "0px 8px 8px 0px", borderLeft: 0, fontSize: "20px", height: "3rem" }} />
                </InputGroup>
              </Col>
            </Row>
            <hr />
            <Row>
              <Col>
                <h5 style={{ color: "#494F66" }}>
                  Agrega o quita coberturas
                </h5>
              </Col>
            </Row>
            <Row></Row>
          </Col>
          <Col md={4} sm={12} lg={4}>
            <br />
            <br />
            <br />
            <br />
            <br />
            <Row>
              <Col style={{ padding: "0 4rem" }}>
                <p>MONTO</p>
                <h3>{(sumaTotal).toLocaleString('en-US', { style: 'currency', currency: 'USD' })}</h3>
                <p>mensuales</p>
              </Col>
            </Row>
            <p style={{
              padding: "0px 3rem"
            }}>
              <hr />
            </p>
            <br />
            <p style={{
              padding: "0px 3rem"
            }}>El precio incluye: </p>
            <p style={{
              padding: "0px 3rem"
            }}>
              <ul >
                <li>Llanta de repuesta</li>
                <li>Análisis de motor</li>
                <li>Aros gratis</li>
              </ul>
            </p>
            <div style={{ padding: "0px 3rem" }} >
              <Input type="button" value="LO QUIERO" onClick={() => history.push("thank")} style={{ backgroundColor: "#EF3340", color: "#FFF", borderRadius: "8px" }} />
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default Step1;

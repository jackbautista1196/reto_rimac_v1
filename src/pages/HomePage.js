import React, { useState } from "react";
import { Container, Row, Col } from "react-grid-system";
import {
  InputGroup,
  InputGroupButtonDropdown,
  DropdownItem,
  FormGroup,
  Label,
  Input,
  DropdownToggle,
  DropdownMenu,
  ButtonToggle,
} from "reactstrap";
//import ReactDatetime from "react-datetime";
import * as utils from "../services/axios-functions";
import "moment/locale/es";
import imageHome from "../assets/images/home.png";
import Header from "../pages/Header";
import { Redirect } from "react-router-dom";

function HomePage(props) {
  const [documentSelect, setDocumentSelect] = useState({
    id: 1,
    descripcion: "DNI",
  });
  const [documents, setDocuments] = useState([
    { id: 1, descripcion: "DNI" },
    { id: 2, descripcion: "Carnet de extranjeria" },
  ]);

  const [checkP, setCheckP] = useState(false);

  const [personSelect, setPersonSelect] = useState([
    {
      n_document: "",
      first_name: "",
      last_name: "",
      last_name_2: "",
      birthdate: "",
      phone: "",
      placa: "",
      gender: "",
    },
  ]);

  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggleDropDown = () => setDropdownOpen(!dropdownOpen);

  const buscar = async () => {
    if (documentSelect.id == 0) {
      alert("Seleccione un tipo de documento");
    } else if (!personSelect.n_document) {
      alert("Ingrese numero de documento");
    } else if (!personSelect.phone) {
      alert("Ingrese un número de telefono");
    } else if (!personSelect.placa) {
      alert("Ingrese número de Placa");
    } else if (!checkP) {
      alert("Debe de aceptar las politicas");
    } else {
      let person = {};
      let document = personSelect.n_document;
      let response = await utils.getUsers(document, "users", 9);
      person = { n_document: personSelect.n_document, phone: personSelect.phone, placa: personSelect.placa };
      localStorage.setItem("response", JSON.stringify(response.data));
      localStorage.setItem("person", JSON.stringify(person));
      //props.history.push(`/step-1/${response.data[0].n_document}`);
      if (response) {
        props.history.push({
          pathname: "/step-1/",
        });
      }
    }
  };

  if (localStorage.getItem("response")) {
    return <Redirect to={"/step-1"} />;
  }

  return (
    <>
      <Header />
      <Container fluid style={{ padding: 0 }}>
        <Row >
          <Col md={4} sm={12} lg={4}>
            <img src={imageHome} alt="Imagen Home" class="img-fluid" />
          </Col>
          <Col md={8} sm={12} lg={8}>
            <div style={{ margin: "0 14rem" }}>
              <br />
              <h3 className="center">
                Déjanos tus datos
              </h3>
              <br />
              <h5 className="center">Ingresa los datos para comenzar</h5>
              <br />
              <Row>
                <Col md={12} sm={12} lg={12}>
                  <InputGroup>
                    <InputGroupButtonDropdown
                      addonType="append"
                      isOpen={dropdownOpen}
                      toggle={toggleDropDown}
                    >
                      <DropdownToggle caret>
                        {documentSelect.descripcion}
                      </DropdownToggle>
                      <DropdownMenu>
                        {documents &&
                          documents.map((item) => (
                            <DropdownItem
                              key={item.id}
                              onSelect={item.id == "1"}
                              onClick={() =>
                                setDocumentSelect({
                                  ...documentSelect,
                                  id: item.id,
                                  descripcion: item.descripcion,
                                })
                              }
                            >
                              {item.descripcion}
                            </DropdownItem>
                          ))}
                      </DropdownMenu>
                    </InputGroupButtonDropdown>
                    <Input
                      style={{
                        borderRadius: "0px 5px 5px 0px"
                      }}
                      type="number"
                      placeholder="Nro. de doc"
                      onChange={(e) =>
                        setPersonSelect({
                          ...personSelect,
                          n_document: e.target.value,
                        })
                      }
                    />
                  </InputGroup>
                </Col>
              </Row>
              <br />
              <Row>
                <Col md={12} sm={12} lg={12}>
                  <Label for="phone">Celular</Label>
                  <InputGroup>
                    <Input
                      style={{
                        borderRadius: "5px"
                      }}
                      id="phone"
                      type="number"
                      placeholder="Ingresa tu numero de celular"
                      onChange={(e) =>
                        setPersonSelect({
                          ...personSelect,
                          phone: e.target.value,
                        })
                      }
                    />
                  </InputGroup>
                </Col>
              </Row>
              <br />
              <Row>
                <Col md={12} sm={12} lg={12}>
                  <Label for="placa">Celular</Label>
                  <InputGroup>
                    <Input
                      style={{
                        borderRadius: "5px"
                      }}
                      id="placa"
                      type="text"
                      placeholder="Placa"
                      onChange={(e) =>
                        setPersonSelect({
                          ...personSelect,
                          placa: e.target.value,
                        })
                      }
                    />
                  </InputGroup>
                </Col>
              </Row>
              <br />
              <Row>
                <Col md={12} sm={12} lg={12}>
                  <FormGroup check>
                    <Label check>
                      <Input
                        type="checkbox"
                        checked={checkP}
                        onChange={() => setCheckP(!checkP)}
                      />{" "}
                      Acepto la <span style={{ textDecorationLine: "underline" }}><a href="#">Política de Protección de Datos Personales</a></span> y los
                      <span style={{ textDecorationLine: "underline" }}> <a href="#">Términos y Condiciones.</a></span>
                    </Label>
                  </FormGroup>
                </Col>
              </Row>
              <br />
              <Row>
                <Col md={12} sm={12} lg={12} className="center-component">
                  <ButtonToggle color="danger" onClick={() => buscar()}>
                    Comencemos
                  </ButtonToggle>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default HomePage;

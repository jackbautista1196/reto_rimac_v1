import React from "react";
import { Container } from "react-grid-system";
import image_header from "../assets/images/logo_rimac.png";
function Header() {
  return (
    <>
      <Container fluid>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            margin: "1rem 5rem",
          }}
        >
          <div>
            <img src={image_header} alt="Imagen Home" class="img-fluid" />
          </div>
          <p>
            ¿Tienes alguna duda?{" "}
            <span>
              <a href="telf:(01)4116001">(01) 411 6001</a>
            </span>
          </p>
        </div>
      </Container>
    </>
  );
}

export default Header;

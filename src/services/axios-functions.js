import util from "../utils/variables";
import axios from "axios";

export function getUsers(document, resources, user_random) {
  console.log(document);
  /*let response = [];
    response = util.person.filter(item => item.n_document == document);

    return JSON.stringify(response);*/

  let config = {
    method: "get",
    url: util.url_api + "/" + resources + "/" + user_random,
    data: [],
  };

  return axios(config)
    .then(async (response) => {
      return response;
    })
    .catch(function (error) {
      alert("Error de conexión: " + error);
    });
}

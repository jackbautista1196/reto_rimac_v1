import "./App.css";
import { HashRouter, Switch, Redirect, Route } from "react-router-dom";
import HomePage from "./pages/HomePage";
import Step1 from "./pages/Step1";
import Thank from "./pages/ThankPage";

function App() {
  return (
    <HashRouter>
      <Switch>
        <Route path="/home" render={(props) => <HomePage {...props} />} />
        <Route path="/step-1" render={(props) => <Step1 {...props} />} />
        <Route path="/thank" render={(props) => <Thank {...props} />} />
      </Switch>
      <Redirect to="/home" />
    </HashRouter>
  );
}

export default App;
